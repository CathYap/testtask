const { defineConfig } = require('@vue/cli-service')
const path = require('path');
module.exports = defineConfig({
  transpileDependencies: true,
  configureWebpack: {
    entry: './src/main.ts',
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: [
            {
              loader: "ts-loader",
              options: {
                appendTsSuffixTo: [/\.vue$/],
              },
            }
          ],
          exclude: /node_modules/,
        },
        {
          test: /\.js/,
          use: [
            {
              loader: "babel-loader",
            }]
        }
      ],
    },
    resolve: {
      extensions: ['*', '.wasm', '.mjs', '.js', '.jsx', '.json', '.vue', '.scss', '.ts', ".jpg", ".png", ".svg"],
    },
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'dist'),
    },
    optimization: {
      splitChunks: {
        chunks: "all",
        minSize: 1,
        minChunks: 10
      }
    }
  }

})
